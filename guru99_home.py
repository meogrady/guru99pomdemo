from locators import Guru99Locators


class Guru99Home(object):

    def __init__(self, driver):
        self.driver = driver

    def get_dashboard_username(self):
        """
        Return the text of the Homepage Username...
        :return:
        """
        return self.driver.find_element(*Guru99Locators.HOMEPAGE_USERNAME).text
