from selenium.webdriver.common.by import By


class Guru99Locators(object):
    """
    This is a single repository for Guru99 locators.
    """

    # Locators used in the Guru99 Login page.
    USER_NAME = (By.NAME, 'uid')
    USER_PASSWORD = (By.NAME, 'password')
    SUBMIT_BUTTON = (By.NAME, 'btnLogin')
    TITLE_TEXT = (By.CLASS_NAME, 'barone')

    # Locators used on the Guru99 Home page
    HOMEPAGE_USERNAME = (By.CSS_SELECTOR, 'tr.heading3')
    LOGIN_TITLE = (By.CSS_SELECTOR, '')
