from locators import Guru99Locators


class Guru99Login(object):
    """
    Guru99Login class provides Login Page functionality...more later...
    """

    def __init__(self, driver):
        self.driver = driver

    def set_user_name(self):
        """
        Sets the user's login name using the locator defined in the
        locators.py file.
        :return:
        """
        uname = self.driver.find_element(*Guru99Locators.USER_NAME)
        uname.clear()
        uname.send_keys('mngr103874')

    def set_password(self):
        """
        Sets the user's password using the locator defined in the
        locators.py file
        :return:
        """
        psword = self.driver.find_element(*Guru99Locators.USER_PASSWORD)
        psword.clear()
        psword.send_keys('ynajynE')

    def click_submit(self):
        """
        Does what it says it does.
        :return:
        """
        self.driver.find_element(*Guru99Locators.SUBMIT_BUTTON).click()

    def get_login_title(self):
        return self.driver.title

    def login_to_guru99(self):
        self.set_user_name()
        self.set_password()
        self.click_submit()
