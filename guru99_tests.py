import unittest
from selenium import webdriver
from guru99_home import Guru99Home
from guru99_login import Guru99Login
import time


class Guru99Tests(unittest.TestCase):

    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(10)
        self.driver.get('http://demo.guru99.com/V4/')

    def test_home_page_should_appear_correct(self):
        login = Guru99Login(self.driver)
        home = Guru99Home(self.driver)
        login_title = login.get_login_title()
        assert "guru99 bank" in login_title.lower()
        login.login_to_guru99()
        home_name = home.get_dashboard_username()
        assert "mngr103874" in home_name
        time.sleep(1)

    def tearDown(self):
        self.driver.close()


if __name__ == '__main__':
    unittest.main()
